<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventbs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 255);
            $table->string('secondname', 255);
            $table->string('phone', 255);
            $table->string('email', 255);
            $table->string('level', 255);
            $table->string('ip', 255);
            $table->text('utmtags');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventbs');
    }
}
