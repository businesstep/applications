<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supperadmin_role   = Role::where('slug','supperadmin')->first();
        $sposora_role       = Role::where('slug', 'sponsora')->first();
        $sposorb_role       = Role::where('slug', 'sponsorb')->first();

        $supperadmin = new User();
        $supperadmin->name = 'суппер админ';
        $supperadmin->email = 'yourname@domain';
        $supperadmin->password = bcrypt('123456');
        $supperadmin->save();
        $supperadmin->roles()->attach($supperadmin_role);

        $sponsora = new User();
        $sponsora->name = 'Спонсор мероприятия А';
        $sponsora->email = 'sponsora@domain';
        $sponsora->password = bcrypt('123456');
        $sponsora->save();
        $sponsora->roles()->attach($sposora_role);

        $sponsorb = new User();
        $sponsorb->name = 'Спонсор мероприятия Б';
        $sponsorb->email = 'sponsorb@domain';
        $sponsorb->password = bcrypt('123456');
        $sponsorb->save();
        $sponsorb->roles()->attach($sposorb_role);
    }
}
