<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supperadmin_role = new Role();
        $supperadmin_role->slug = 'supperadmin';
        $supperadmin_role->name = 'Суппер администратор';
        $supperadmin_role->save();

        $sposora_role = new Role();
        $sposora_role->slug = 'sponsora';
        $sposora_role->name = 'Организатор мероприятия А';
        $sposora_role->save();

        $sposorb_role = new Role();
        $sposorb_role->slug = 'sponsorb';
        $sposorb_role->name = 'Организатор мероприятия Б';
        $sposorb_role->save();
    }
}
