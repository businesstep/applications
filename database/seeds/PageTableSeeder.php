<?php

use Illuminate\Database\Seeder;
use App\Page;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new Page();
        $page->name     = 'Главная страница';
        $page->slug     = 'home';
        $page->content  = '<h1>Дополнительные инструкции</h1><p>Реализовать главную страницу с произвольным контентом - текст + картинки. При клике на картинку, она отображается в увеличенном виде во всплывающем окне.</p><p>В панели администрирования суперадминистратор должен иметь возможность редактировать контент главной страницы</p>';
        $page->save();
    }
}
