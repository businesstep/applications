<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('eventa', 'EventAController@create');
Route::post('eventa', 'EventAController@store');

Route::get('eventb', 'EventBController@create');
Route::post('eventb', 'EventBController@store');

//Auth::routes();
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>'isadmin'], function() {
    Route::get('/main', 'AdminController@index')->name('admin');

    Route::get('pages', 'PagesController@index');
    Route::get('pages/create', 'PagesController@create');
    Route::post('pages/create', 'PagesController@store');

    Route::get('pages/{slug}/edit', 'PagesController@edit');
    Route::post('pages/{slug}/edit', 'PagesController@update');

    Route::get('images/{slug}/create', 'ImagesController@create');
    Route::post('images/{slug}/create', 'ImagesController@store');

    Route::get('roles', 'RolesController@index');
    Route::get('roles/create', 'RolesController@create');
    Route::post('roles/create', 'RolesController@store');

    Route::get('applications', 'ApplicationsController@index');
    Route::get('applications/a/delete/{id}', 'ApplicationsController@destroya');
    Route::get('applications/b/delete/{id}', 'ApplicationsController@destroyb');

    Route::get('users', 'UsersController@index');
    Route::get('users/{id}/edit', 'UsersController@edit');
    Route::post('users/{id}/edit','UsersController@update');

});

Route::get('sendemail', function () {
    $data = array( 'name' => "Applications test site", );
    /*
    Mail::send('emails.welcome', $data, function ($message) {
        $message->from('your_email', 'Laravel Отправитель');
        $message->to('your_email')->subject('Проверка отправки почты');
    });
    */

    dispatch(new App\Jobs\SendEmailJob($data));

    return "Письмо успешно отправлено!";
});
