<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Page extends Model
{
    protected $fillable = ['name', 'slug', 'content'];

    public function images()
    {
        return $this->hasMany('App\Image', 'page_id');
    }
}
