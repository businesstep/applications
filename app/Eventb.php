<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventb extends Model
{
    protected $fillable = ['firstname', 'secondname', 'phone', 'email', 'level', 'ip', 'utmtags'];
}
