<?php
namespace App\Permissions;

use App\Permission;
use App\Role;

trait HasPermissionsTrait
{
    /**
     * Setup user relations
     *
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * Setup user relations
     *
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'users_permissions');
    }

    /**
     * Checking roles by slug
     *
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(... $roles)
    {
        foreach($roles as $role)
        {
            if($this->roles->contains('slug', $role))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Iterating through each permission associated with a role
     *
     * @param object $permission
     * @return bool
     */
    public function hasPermissionThroughRole($permission)
    {
        foreach ($permission->roles as $role)
        {
            if($this->roles->contains($role))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Checking permission by slug
     *
     * @param object $permission
     * @return bool
     */
    protected function hasPermission($permission)
    {
        return (bool)$this->permissions->where('slug', $permission->slug)->count();
    }

    /**
     * Checking permissions
     *
     * @param object $permission
     * @return bool
     */
    public function hasPermissionTo($permission)
    {
        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }
}