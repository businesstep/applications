<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Image;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as ImageLib;

class ImagesController extends Controller
{
    public function create($slug)
    {
        $page 	= Page::whereSlug($slug)->firstOrFail();
		
        return view('admin.images.create', ['page' => $page,]);
    }

    public function store(Request $request)
    {
        $slug       = $request->get('page_slug');

        try
		{
            $this->validate($request, [
                'name' => 'required'
            ]);

			$imageFile  = $request->file('image');

			$fileNameToStore = $request->get('name').'.'.$imageFile->getClientOriginalExtension();

			$thumbPath = public_path('images/pages/'.$slug.'/thumbs/');

			if (!file_exists($thumbPath)) {
				mkdir($thumbPath, 666, true);
			}

			ImageLib::make($imageFile->getRealPath())
				->resize(300, 200)
				->save($thumbPath.$fileNameToStore);

			$imagePath = public_path('images/pages/'.$slug);
			$imageFile->move($imagePath, $fileNameToStore);

			$page = Page::whereSlug($slug)->firstOrFail();

			$image = new Image([
				'name'      => $fileNameToStore,
				'page_id'   => $page->id,
			]);

			$image->save();
		}
		catch(\PostTooLargeException $ex)
        {
            return redirect('/admin/images/'.$slug.'/create')->with('status','Изображение не удалось добавить!');
        }
		catch(\Exception $ex)
		{
			return redirect('/admin/images/'.$slug.'/create')->with('status','Изображение не удалось добавить!');
		}
		
        return redirect('/admin/images/'.$slug.'/create')->with('status','Изображение добавлено!');
    }
}
