<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', ['users' => $users]);
    }

    public function edit($id)
    {
        $user   = User::whereId($id)->firstOrFail();
        $roles  = Role::all();

        return view('admin.users.edit', ['user'=>$user, 'roles' => $roles]);
    }

    public function update($id, Request $request)
    {
        $user = User::whereId($id)->firstOrFail();

        $user->name     = $request->get('name');
        $user->email    = $request->get('email');

        $user->save();

        $user->saveRoles($request->get('role'));

        return redirect(action('Admin\UsersController@index'))->with('status','Обновление выполнено!');
    }
}
