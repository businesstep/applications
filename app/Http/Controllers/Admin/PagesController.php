<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Show the list of pages
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = Page::all();

        return view('admin.pages.index', ['pages' => $pages]);
    }

    /**
     * Show form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Adding the data of page to DB
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
			'slug' => 'required',
        ]);

        $slug = $request->get('slug');

        $page = new Page([
            'name'     => $request->get('name'),
            'slug'     => $request->get('slug'),
            'content'  => $request->get('content')
        ]);

        $page->save();

        return redirect(action('Admin\PagesController@create'))->with('status', 'Страница добавлена в БД. Slug: '.$slug);
    }

    /**
     * Show form
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($slug)
    {
        $page = Page::whereSlug($slug)->firstOrFail();

        return view('admin.pages.edit', ['page'=>$page]);
    }

    /**
     * Updating the content text of page in DB
     *
     * @param string $slug
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($slug, Request $request)
    {
        $page = Page::whereSlug($slug)->firstOrFail();

        $page->content = $request->get('content');
        $page->save();

        return redirect(action('Admin\PagesController@edit', $page->slug))->with('status','Обновление выполнено!');
    }
}
