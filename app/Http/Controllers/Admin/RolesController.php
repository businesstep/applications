<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return view('admin.roles.index', ['roles' => $roles]);
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $slug = $request->get('slug');

        $role = new Role([
            'name'     => $request->get('name'),
            'slug'     => $request->get('slug')
        ]);

        $role->save();

        return redirect(action('Admin\RolesController@create'))->with('status', 'Роль добавлена в БД. Slug: '.$slug);
    }
}
