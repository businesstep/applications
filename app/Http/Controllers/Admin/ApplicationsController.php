<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Eventa;
use App\Eventb;

class ApplicationsController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        if($user->hasRole('sponsora'))
        {
            $applicationsA = Eventa::all();
            $applicationsB = [];
        }
        else if($user->hasRole('sponsorb'))
        {
            $applicationsA = [];
            $applicationsB = Eventb::all();
        }
        else if($user->hasRole('supperadmin'))
        {
            $applicationsA = Eventa::all();
            $applicationsB = Eventb::all();
        }
        else
        {
            $applicationsA = [];
            $applicationsB = [];
        }

        return view('admin.applications.index', ['applicationsa' => $applicationsA, 'applicationsb' => $applicationsB]);
    }

    public function destroya($id)
    {
        $application = Eventa::find($id);
        $application->delete();

        return redirect('/admin/applications')->with('status', 'Заявка №' . $id . ' удалена!');
    }

    public function destroyb($id)
    {
        $application = Eventb::find($id);
        $application->delete();

        return redirect('/admin/applications')->with('status', 'Заявка №' . $id . ' удалена!');
    }
}
