<?php

namespace App\Http\Controllers;

use App\Eventa;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendEmailJob;

class EventAController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }

    /**
     * Show form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('eventa.create');
    }

    /**
     * Store to DB and send mail
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $eventa = new Eventa([
            'firstname'     => $request->get('firstname'),
            'secondname'    => $request->get('secondname'),
            'phone'         => $request->get('phone'),
            'email'         => $request->get('email'),
            'level'         => $request->get('level'),
            'ip'            => $request->ip(),
            'utmtags'       => response()->json($request->query()),
        ]);

        $eventa->save();

        // send to user
        $data = [   'view'      => 'emails.applications',
                    'event'     => 'Заявка на мероприятие А',
                    'to'        => $request->get('email'),
                    'subject'   => 'Заявка на мероприятие А',
                    'user'      => $request->get('firstname')
        ];

        dispatch(new SendEmailJob($data));

        /*
        Mail::send('emails.applications', $data, function ($message) use ($data) {
            $message->from('local@localhost', 'Laravel - Заявки');
            $message->to($data['to'])->subject('Заявка на мероприятие А');
        });
        */

        // send to sponsor
        $role = Role::whereSlug('sponsora')->firstOrFail();

        $users = User::whereHas('roles', function($query) use($role) {
            $query->where('role_id', $role->id);
        })->with('roles')->get();

        $sponsors = [];
        foreach($users as $user)
        {
            $sponsors[] = $user->email;
        }

        $data = [   'view'      => 'emails.sponsors',
                    'event'     => 'У вас новая заявка',
                    'to'        => implode(';', $sponsors),
                    'subject'   => 'Заявка на мероприятие А',
                    'user'      => $request->get('firstname')
        ];

        dispatch(new SendEmailJob($data));

        /*
        Mail::send('emails.sponsors', $data, function ($message) use ($data) {
            $message->from('local@localhost', 'Laravel - Заявки');
            $message->to($data['to'])->subject('Заявка на мероприятие А');
        });
        */

        //return redirect('/eventa')->with('status', 'Заявка принята. На email отправлено подтверждение.');
        return [
            'type'  => "ok",
            'value' => "Заявка принята. На email отправлено подтверждение.",
        ];
    }
}

