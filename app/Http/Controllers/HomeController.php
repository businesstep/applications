<?php

namespace App\Http\Controllers;

use App\Image;
use App\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['guest', 'auth', 'isadmin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page = Page::whereSlug('home')->firstOrFail();

        return view('home.index', ['page'=>$page]);
    }
}
