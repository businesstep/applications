<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Page;

class Image extends Model
{
    protected $fillable= ['name', 'page_id'];

    public function page()
    {
        return $this->belongsTo('App\Page');
    }
}
