@extends('admin.layout')

@section('title', 'Загруза изображения')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="list-group-item-heading">Добавляем в базу</h4>
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }}</p>
                @endforeach

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @csrf

                <fieldset>
                    <div class="form-group">
                        <label for="title" class="col-lg-2 control-label">Название</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="title" required name="name" placeholder="Название" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-lg-2 control-label">Upload</label>
                        <div class="col-md-3">
                            <input type="file" class="form-control" id="image" name="image" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <input type="hidden" id="page_slug" name="page_slug" value="{!! $page->slug !!}" />
                            <button type="submit" class="btn btn-primary">Загрузить</button>
                            <a class="btn btn-default" href="/admin/pages">Вернуться к списку</a>
                        </div>
                    </div>
                </fieldset>
            </form>
			@if(count($page->images) > 0 )
				<div class="row">
					<div class="col-md-8">
						<strong>Оригинальное изображение:</strong>
					</div>
					<div class="col-md-4">
						<strong>Уменьшенное изображение:</strong>
					</div>
				</div>
				@foreach($page->images as $image)
					<div class="row">
						<div class="col-md-8">
							<img src="{!! asset('images/pages/').'/'.$page->slug.'/'.$image->name !!}" />
						</div>
						<div class="col-md-4">
							<img src="{!! asset('images/pages/').'/'.$page->slug.'/thumbs/'.$image->name !!}"  />
						</div>
					</div>
				@endforeach
			@endif  
        </div>
    </div>
@endsection