@extends('admin.layout')

@section('title', 'Редактирование Пользователя')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="list-group-item-heading">{!! $user->name !!}</h4>
            <form class="form-horizontal" method="post">
                @if (session('status'))
                    <div class="alert alert-success"> {{ session('status') }}</div>
                @endif

                @csrf

                <div class="form-group">
                    <label for="name" class="col-lg-2 control-label">Имя</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="name" autofocus name="name" value="{!! $user->name !!}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-lg-2 control-label">E-mail</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="email" name="email" value="{!! $user->email !!}" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10">
                        <select class="form-control" id="role" name="role[]" multiple>
                            @foreach($roles as $role)
                                <option value="{!! $role->id !!}" >
                                    {!! $role->name !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Обновить</button>
                        <a class="btn btn-default" href="/admin/users">Вернуться к списку</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection