@extends('admin.layout')

@section('title', 'Список Пользователей')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <h4 class="list-group-item-heading">Пользователи</h4>
            @if ($users->isEmpty())
                <p>Таблица Пользователей пока пуста.</p>
            @else
                <div class="row table">
                    <div class="col-md-1">№</div>
                    <div class="col-md-2">Имя</div>
                    <div class="col-md-2">Email</div>
                    <div class="col-md-7">Роль</div>
                </div>
                @foreach($users as $user)
                    <div class="row">
                        <div class="col-md-1">{!! $user->id !!}</div>
                        <div class="col-md-2"><a href="/admin/users/{!! $user->id !!}/edit">{!! $user->name !!}</a></div>
                        <div class="col-md-2">{!! $user->email !!}</div>
                        <div class="col-md-7">
                            @foreach($user->roles as $role)
                                <small>{{ $role->name }}</small>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection