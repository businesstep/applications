@extends('admin.layout')

@section('title', 'Редактирование страниц')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="list-group-item-heading">{!! $page->name !!}</h4>
            <form class="form-horizontal" method="post">
                @if (session('status'))
                    <div class="alert alert-success"> {{ session('status') }}</div>
                @endif

                @csrf

                <div class="form-group">
                    <label for="content" class="col-lg-2 control-label">Контент страницы</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" id="content" name="content">{!! $page->content !!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Обновить</button>
                        <a class="btn btn-default" href="/admin/pages">Вернуться к списку</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection