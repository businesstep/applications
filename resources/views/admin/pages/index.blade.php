@extends('admin.layout')

@section('title', 'Редактирование страниц')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <h4 class="list-group-item-heading">Страницы</h4>
            @if ($pages->isEmpty())
                <p>Таблица Страниц пока пуста. <a href="/admin/pages/create">Создать</a></p>
            @else
                <div class="row table">
                    <div class="col-md-1">№</div>
                    <div class="col-md-2">Название</div>
                    <div class="col-md-2">Slug</div>
                    <div class="col-md-3"></div>
                </div>
                @foreach($pages as $page)
                    <div class="row">
                        <div class="col-md-1">{!! $page->id !!}</div>
                        <div class="col-md-2"><a href="/admin/pages/{!! $page->slug !!}/edit">{!! $page->name !!}</a></div>
                        <div class="col-md-2">{!! $page->slug!!}</div>
                        <div class="col-md-3">
                            <a href="/admin/images/{!! $page->slug !!}/create"><small>Загрузить изображения</small></a>
                        </div>
                    </div>
                @endforeach
                <div class="row table">
                    <div clas="col-md-10">
                        <hr>
                        <a href="/admin/pages/create">Создать новую страницу</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection