@extends('admin.layout')

@section('title', 'Админ панель')

@section('content')
    <div class="row">
        @role('supperadmin')
            <div class="col-md-4">
                <div class="card" style="width: 18rem;height:13rem;">
                    <div class="card-body">
                        <h5 class="card-title">Управление Страницами</h5>
                        <p class="card-text">В разделе возможно создавать станицы, редактировать контент страниц, загружать изображения.</p>
                        <a class="btn btn-info" href="/admin/pages">Открыть</a>
                    </div>
                </div>
            </div>
        @endrole
        <div class="col-md-4">
            <div class="card" style="width: 18rem;height:13rem;">
                <div class="card-body">
                    <h5 class="card-title">Управление Ролями</h5>
                    <p class="card-text">Справочник ролей. Возможно создавать роли.</p>
                    <a class="btn btn-info" href="/admin/roles">Открыть</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width: 18rem;height:13rem;">
                <div class="card-body">
                    <h5 class="card-title">Управление Заявками</h5>
                    <p class="card-text">Заявки на мероприятия. Возможно удалять заявки.</p>
                    <a class="btn btn-info" href="/admin/applications">Открыть</a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" style="width: 18rem;height:13rem;">
                <div class="card-body">
                    <h5 class="card-title">Управление Пользователями</h5>
                    <p class="card-text">Раздел менеджеров.</p>
                    <a class="btn btn-info" href="/admin/users">Открыть</a>
                </div>
            </div>
        </div>
    </div>
@endsection