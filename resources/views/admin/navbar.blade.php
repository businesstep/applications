<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="row">
        <div class="col-md-8">
            <a class="navbar-brand" href="/">На сайт</a>
            <a class="navbar-brand" href="/admin/main">Админ панель</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        @if (Auth::check())
            <a class="dropdown-item" style="float:right;" href="/logout">Выйти</a>
        @endif
    </div>
</nav>