@extends('admin.layout')

@section('title', 'Список Ролей')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <h4 class="list-group-item-heading">Роли</h4>
            @if ($roles->isEmpty())
                <p>Таблица Ролей пока пуста. <a href="/admin/roles/create">Создать</a></p>
            @else
                <div class="row table">
                    <div class="col-md-1">№</div>
                    <div class="col-md-2">Название</div>
                    <div class="col-md-2">Slug</div>
                </div>
                @foreach($roles as $role)
                    <div class="row">
                        <div class="col-md-1">{!! $role->id !!}</div>
                        <div class="col-md-2">{!! $role->name !!}</div>
                        <div class="col-md-2">{!! $role->slug!!}</div>
                    </div>
                @endforeach
                <div class="row table">
                    <div clas="col-md-10">
                        <hr>
                        <a href="/admin/roles/create">Создать Роль</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection