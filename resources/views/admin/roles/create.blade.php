@extends('admin.layout')

@section('title', 'Создание Роли')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="list-group-item-heading">Добавляем в базу</h4>
            <form class="form-horizontal" method="post">
                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }}</p>
                @endforeach

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @csrf

                <div class="form-group row">
                    <label for="title" class="col-lg-2 control-label">Название</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="title" name="name" placeholder="Название" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="slug" class="col-lg-2 control-label">Slug</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Создать</button>
                        <a class="btn btn-default" href="/admin/roles">Вернуться к списку</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection