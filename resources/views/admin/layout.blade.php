<!doctype html>
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <link rel="stylesheet" type="text/css" href="{!! asset('css/admin.css') !!}" >
</head>
<body>
    @include('admin.navbar')

    <div class="container content">
        @yield('content')
    </div>

    <script src="{!! asset('js/admin.js') !!}"></script>
</body>
</html>