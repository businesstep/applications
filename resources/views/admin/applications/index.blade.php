@extends('admin.layout')

@section('title', 'Заявки на мероприятия')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success"> {{ session('status') }}</div>
            @endif

            @if( count($applicationsa) > 0 )
                <h4 class="list-group-item-heading">Заявки Мероприятия А</h4>
                <div class="row table">
                    <div class="col-md-2">№</div>
                    <div class="col-md-2">ФИО</div>
                    <div class="col-md-2">телефон</div>
                    <div class="col-md-2">e-mail</div>
                    <div class="col-md-2">уровень образования</div>
                    <div class="col-md-2"></div>
                </div>
                @foreach($applicationsa as $application)
                    <div class="row" style="background-color: #e2e6ea;">
                        <div class="col-md-2">{!! $application->id !!}</div>
                        <div class="col-md-2">{!! $application->secondname !!} {!! $application->firstname !!}</div>
                        <div class="col-md-2">{!! $application->phone !!}</div>
                        <div class="col-md-2">{!! $application->email !!}</div>
                        <div class="col-md-2">{!! $application->level !!}</div>

                        @role('supperadmin')
                            <div class="col-md-2">
                                <a href="/admin/applications/a/delete/{!! $application->id !!}" onclick="return confirm('Действительно удалить?');"><small>Удалить</small></a>
                            </div>
                        @endrole
                    </div>

                    @role('supperadmin')
                        <div class="row">
                            <div class="col-md-2">{!! $application->ip !!}</div>
                            <div class="col-md-10">{!! $application->utmtags !!}</div>
                        </div>
                    @endrole
                    <hr>
                @endforeach
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success"> {{ session('status') }}</div>
            @endif

            @if( count($applicationsb) > 0 )
                <h4 class="list-group-item-heading">Заявки Мероприятия Б</h4>
                <div class="row table">
                    <div class="col-md-2">№</div>
                    <div class="col-md-2">ФИО</div>
                    <div class="col-md-2">телефон</div>
                    <div class="col-md-2">e-mail</div>
                    <div class="col-md-2">уровень образования</div>
                    <div class="col-md-2"></div>
                </div>
                @foreach($applicationsb as $application)
                    <div class="row" style="background-color: #e2e6ea;">
                        <div class="col-md-2">{!! $application->id !!}</div>
                        <div class="col-md-2">{!! $application->secondname !!} {!! $application->firstname !!}</div>
                        <div class="col-md-2">{!! $application->phone !!}</div>
                        <div class="col-md-2">{!! $application->email !!}</div>
                        <div class="col-md-2">{!! $application->level !!}</div>

                        @role('supperadmin')
                            <div class="col-md-2">
                                <a href="/admin/applications/b/delete/{!! $application->id !!}" onclick="return confirm('Действительно удалить?');"><small>Удалить</small></a>
                            </div>
                        @endrole
                    </div>
                    @role('supperadmin')
                        <div class="row">
                            <div class="col-md-2">{!! $application->ip !!}</div>
                            <div class="col-md-10">{!! $application->utmtags !!}</div>
                        </div>
                    @endrole
                    <hr>
                @endforeach
            @endif
        </div>
    </div>
@endsection