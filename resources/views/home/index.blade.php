@extends('layouts.app')

@section('title', 'Главная')

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(count($page->images) > 0 )
                        <div class="gallery">
                            @foreach($page->images as $image)
                                <a href="{!! asset('images/pages/').'/'.$page->slug.'/'.$image->name !!}" data-caption="{!! $image->name !!}">
                                    <img src="{!! asset('images/pages/').'/'.$page->slug.'/thumbs/'.$image->name !!}" alt="{!! $image->name !!}" />
                                </a>
                            @endforeach
                        </div>
                    @else
                        <p>Изображений для страницы не добавлено. Необходимо перйти в Админ панель, в раздел "Управление страницами" и добавить изображения.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
