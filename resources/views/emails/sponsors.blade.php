<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>{!! $event !!}</h2>
        <p>Имя: {!! $application->firstname !!}</p>
        <p>Фамилия: {!! $application->secondname !!}</p>
        <p>Телефон: {!! $application->phone !!}</p>
        <p>E-mail: {!! $application->email !!}</p>
        <p>Уровень образования: {!! $application->level !!}</p>
    </body>
</html>