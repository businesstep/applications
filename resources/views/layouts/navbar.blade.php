<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Laravel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/"><i class="fas fa-archway"></i>Главная</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Заявки</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/eventa">Мероприятие А</a>
                    <a class="dropdown-item" href="/eventb">Меропирятие Б</a>
                </div>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-center">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--i class="fas fa-door-open"></i-->Админ панель</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @if (Auth::check())
                            <a class="dropdown-item" style="float:right;" href="/admin/main">Админ.панель</a>
                            <a class="dropdown-item" href="/logout">Выйти</a>
                        @else
                            <a class="dropdown-item" href="/login">Авторизация</a>
                            <a class="dropdown-item" href="/register">Регистрация</a>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>