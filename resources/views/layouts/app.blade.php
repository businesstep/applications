<!doctype html>
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title> @yield('title') </title>

        <link rel="stylesheet" type="text/css" href="{!! asset('css/app.css') !!}" >
        <link rel="stylesheet" type="text/css" href="{!! asset('css/style.css') !!}" >
    </head>
    <body>
        @include('layouts.navbar')

        @yield('content')

        <script src="{!! asset('js/bootstrap.js') !!}"></script>
    </body>
</html>