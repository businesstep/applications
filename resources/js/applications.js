class Applications
{
    addApplication(url, firstname, secondname, phone, email, level)
    {
        $.ajax({
            method: "POST",
            url: url,
            data: { firstname: firstname, secondname: secondname, phone: phone, email: email, level: level }
        })
        .done(function(message) {
            console.log('ajax: add application - OK!');

            $(".card-body").prepend("<div class='alert alert-success'>" + message.value + "</div>");
        });
    }
}

export let applications = new Applications();